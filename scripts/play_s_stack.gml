/// @function play_s_stack(Sound)
/// @description play an elment from a sounds list.
/// @param Sound List.

        var sound = ds_list_find_value(argument0,0);
        ds_list_delete(argument0, 0);
        ds_list_add(argument0, sound);
        audio_play_sound(sound, 100, false);
        return true;



