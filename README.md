Global Game Jam Game (2018)

Battle Blood it's a exploring / Beat 'em up / survival game. You are a bacteria trying to find your way out of the blood strain. All aesthetics is intentionally retro 8 bit pixel art style, all original music and sounds also match this style.

https://globalgamejam.org/2018/games/battle-blood


